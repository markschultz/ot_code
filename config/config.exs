# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :ot_code,
  ecto_repos: [OtCode.Repo]

# Configures the endpoint
config :ot_code, OtCodeWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "MjXWXND5fVjO4cLQRdX3SIMSkc8Xf7cxlGGg1VlZ+CUbFHVeGM5YLhIBvPjWrb67",
  render_errors: [view: OtCodeWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: OtCode.PubSub,
  live_view: [signing_salt: "Sk1vnc9N"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
