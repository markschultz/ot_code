defmodule OtCodeWeb.CodeLive.Show do
  use OtCodeWeb, :live_view

  alias OtCode.Chatrooms
  alias OtCodeWeb.Presence

  defp topic(chat_id), do: "chat:#{chat_id}"

  @impl true
  def mount(%{"id" => id}, _session, socket) do
    user_id = OtCode.StringGenerator.string_of_length(10)

    if connected?(socket) do
      Presence.track(self(), topic(id), user_id, %{
        online_at: inspect(System.system_time(:second)),
        nickname: "Anonymous"
      })

      OtCodeWeb.Endpoint.subscribe(topic(id))
    end

    users =
      Presence.list(topic(id))
      |> Enum.map(fn {_user_id, data} ->
        data[:metas]
        |> List.first()
      end)

    socket =
      assign(socket,
        user_id: user_id,
        nickname: "",
        room_id: id,
        messages: [],
        users: users,
        message: Chatrooms.change_message()
      )

    {:ok, socket}
  end

  @impl true
  def handle_params(%{"id" => id}, _, socket) do
    {:noreply,
     socket
     |> assign(:page_title, "Code Editor: " <> id)}
  end

  @impl true
  def handle_event("message", %{"message" => message_params}, socket) do
    chat = Chatrooms.create_message(message_params)
    messages = [chat | socket.assigns.messages]

    OtCodeWeb.Endpoint.broadcast_from(self(), topic(message_params["chat_id"]), "message", %{
      messages: messages
    })

    {:noreply,
     assign(socket,
       messages: messages,
       message: Chatrooms.change_message()
     )}
  end

  def handle_event("nickname", %{"nickname" => nickname}, socket) do
    Presence.update_presence(self(), topic(socket.assigns.room_id), socket.assigns.user_id, %{
      nickname: if(nickname == "", do: "Anonymous", else: nickname)
    })

    {:noreply, assign(socket, nickname: nickname)}
  end

  @impl true
  def handle_info(%{event: "message", payload: state}, socket) do
    {:noreply, assign(socket, state)}
  end

  def handle_info(
        %{event: "presence_diff", payload: _payload},
        socket = %{assigns: %{room_id: room_id}}
      ) do
    users = Presence.list_presences(topic(room_id))

    {:noreply, assign(socket, users: users)}
  end
end
