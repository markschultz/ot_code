defmodule OtCodeWeb.CodeLive.Index do
  use OtCodeWeb, :live_view
  alias OtCode.Chatrooms
  alias OtCode.Chatrooms.Chat

  @impl true
  def mount(_params, _session, socket) do
    changeset = Chatrooms.change_chat(%Chat{})
    {:ok, assign(socket, changeset: changeset)}
  end

  @impl true
  def handle_params(_, _, socket) do
    {:noreply,
     socket
     |> assign(:page_title, "Code Collab")}
  end

  @impl true
  def handle_event("create", %{"chat" => %{"name" => query}}, socket) do
    if query == nil || String.length(String.trim(query)) == 0 do
      name = OtCode.StringGenerator.string_of_length(6)
      {:noreply, push_redirect(socket, to: Routes.code_show_path(socket, :show, name))}
    else
      {:noreply, push_redirect(socket, to: Routes.code_show_path(socket, :show, query))}
    end
  end

  def handle_event("validate", %{"chat" => chat_params}, socket) do
    changeset =
      %Chat{}
      |> Chatrooms.change_chat(chat_params)
      |> Map.put(:action, :validate)

    {:noreply, assign(socket, :changeset, changeset)}
  end
end
