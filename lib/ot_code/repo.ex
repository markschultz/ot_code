defmodule OtCode.Repo do
  use Ecto.Repo,
    otp_app: :ot_code,
    adapter: Ecto.Adapters.Postgres
end
