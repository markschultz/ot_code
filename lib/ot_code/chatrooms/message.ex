defmodule OtCode.Chatrooms.Message do
  use Ecto.Schema
  import Ecto.Changeset

  schema "messages" do
    field :content, :string
  end

  @doc false
  def changeset(chat, attrs) do
    chat
    |> cast(attrs, [:content])
    |> validate_required([:content])
  end
end
