defmodule OtCode.StringGenerator do
  @chars "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvxyz" |> String.codepoints()

  def string_of_length(length) do
    1..length |> Enum.map(fn _i -> Enum.random(@chars) end) |> Enum.join("")
  end
end
