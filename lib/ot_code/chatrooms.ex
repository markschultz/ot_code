defmodule OtCode.Chatrooms do
  @moduledoc """
  The Chatrooms context.
  """

  import Ecto.Query, warn: false

  alias OtCode.Chatrooms.Chat
  alias OtCode.Chatrooms.Message

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking message changes.

  ## Examples

      iex> change_message(message)
      %Ecto.Changeset{data: %Message{}}

  """
  def change_message(%Message{} = message \\ %Message{}, attrs \\ %{}) do
    Message.changeset(message, attrs)
  end

  def create_message(params) do
    %{
      from_name:
        if(params["from_name"] == "",
          do: "Anonymous",
          else: params["from_name"]
        ),
      timestamp: Time.utc_now() |> Time.truncate(:second) |> Time.to_string(),
      content: params["content"]
    }
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking chat changes.

  ## Examples

      iex> change_chat(chat)
      %Ecto.Changeset{data: %Chat{}}

  """
  def change_chat(%Chat{} = chat \\ %Chat{}, attrs \\ %{}) do
    Chat.changeset(chat, attrs)
  end
end
